package info.hccis.teetimebooker.dao;

import info.hccis.teetimebooker.entity.Booking;
import info.hccis.util.CisUtility;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Class with methods related to Booking Database class
 *
 * @author bjm
 * @since 20200521
 */
public class BookingDAO {

    /**
     * Insert a booking into the database.
     * @since 2020-05-21
     * @author BJM
     */
    public void insert(Booking booking) {

        Connection conn = null;

        try {
            conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/cis2232_teetime",
                    "root",
                    "");
        } catch (SQLException ex) {
            Logger.getLogger(BookingDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

        //***************************************************
        // INSERT
        //***************************************************
        try {
            String theStatement = "INSERT INTO Booking(id, name1, name2, name3, "
                    + "name4, bookingDate, bookingTime, courseName, createdDateTime) "
                    + "VALUES (0,?,?,?,?,?,?,?,?)";
            PreparedStatement stmt = conn.prepareStatement(theStatement);
            stmt.setString(1, booking.getName1());
            stmt.setString(2, booking.getName2());
            stmt.setString(3, booking.getName3());
            stmt.setString(4, booking.getName4());
            stmt.setString(5, booking.getDateOfBooking());
            stmt.setString(6, booking.getTimeOfBooking());
            stmt.setString(7, booking.getCourseName());
            stmt.setString(8, CisUtility.getCurrentDate("yyyy-MM-dd hh:mm"));

            stmt.executeUpdate();

        } catch (SQLException sqle) {
            System.out.println("sql exception caught");
            sqle.printStackTrace();
        }

    }

    /**
     * Select all bookings from the database
     * @since 2020-05-21
     * @author BJM
     */
    public ArrayList<Booking> selectAll() {
        ArrayList<Booking> bookings = new ArrayList();
        try {
            Connection conn = null;
            conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/cis2232_teetime",
                    "root",
                    "");

            Statement stmt = conn.createStatement();

            //***************************************************
            // Select using statement
            //***************************************************
            //Next select all the rows and display them here...
            ResultSet rs = stmt.executeQuery("select * from Booking");

            //Show all the bookers
            while (rs.next()) {

                int id = rs.getInt("id");
                String name1 = rs.getString("name1");
                String name2 = rs.getString("name2");
                String name3 = rs.getString("name3");
                String name4 = rs.getString("name4");
                String bookingDate = rs.getString("bookingDate");
                String bookingTime = rs.getString("bookingTime");
                String courseName = rs.getString("courseName");
                Booking booking = new Booking(id, name1, name2, name3, name4, courseName, bookingDate, bookingTime);
                bookings.add(booking);
                System.out.println("Booker for id: " + rs.getString("id") + " is " + rs.getString("name1"));
            }
        } catch (SQLException ex) {
            System.out.println("Error selecting bookings. (" + ex.getMessage() + ")");

        }
        return bookings;

    }
}

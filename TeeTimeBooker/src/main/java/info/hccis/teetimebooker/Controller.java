package info.hccis.teetimebooker;

import info.hccis.teetimebooker.bo.BookingBO;
import info.hccis.util.CisUtility;

/**
 * Controller class
 *
 * @author bjm
 * @since 20200514
 */
public class Controller {

    public static final String EXIT = "X";

    private static final String MENU
            = "-------------------------\n"
            + "- CIS Menu\n"
            + "- A-Add a booking\n"
            + "- B-Show bookings\n"
            + "- C-Update booking\n"
            + "- X-eXit\n"
            + "-------------------------\n"
            + "Option-->";

    private static BookingBO bookingBO = new BookingBO();
    
    public static void main(String[] args) {

        //Get the bookings loaded.  
        bookingBO.loadBookings();

        //Add a loop below to continuously promput the user for their choice 
        //until they choose to exit.
        String option = "";

        CisUtility.display("Tee Time Booker"
                + "\nToday is: " + CisUtility.getCurrentDate(null));
        do {
            option = CisUtility.getInputString(MENU);
            processMenuOption(option);
        } while (!option.equalsIgnoreCase(EXIT));

    }


    /**
     * This method will process the menu option specified in the input
     * parameter. It will call appropriate functionality based on its value.
     *
     * @param option The menu option
     * @since 20171102
     * @author cis1201b
     *
     */
    public static void processMenuOption(String option) {
        //Add a switch to process the option
        switch (option.toUpperCase()) {
            case "A":
                CisUtility.display("Add a booking");
                bookingBO.addABooking();
                break;
            case "B":
                CisUtility.display("Here are the bookings");
                bookingBO.showBookings();
                break;
            case "C":
                CisUtility.display("Future Functionality (Update)");
//                bookingBO.updateBooking();
                break;           
            case "X":
                CisUtility.display("User picked x");
                break;
            default:
                CisUtility.display("Invalid entry");
        }
    }


}

package com.mycompany.testjdbcb;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

/**
 * This class will be our initial connection to a database.
 *
 * @since Sep 27, 2017
 * @author bjmaclean
 */
public class TestJdbcMain {

    public static void main(String[] args) {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/cis2232_teetime",
                    "root",
                    "");

            //***************************************************
            // INSERT
            //***************************************************
            
            
            
            Statement stmt = conn.createStatement();
            try {
                
                String theStatement = "INSERT INTO Booking(id, name1, name2, name3, "
                        + "name4, bookingDate, bookingTime, courseName, createdDateTime) "
                        + "VALUES (0,'Pierre','Julie','Marc',''"
                        + ",'2020-05-20','16:00','Fox Meadow','2020-05-21 09:43')";
                
                stmt.executeUpdate(theStatement);

            } catch (SQLException sqle) {
                System.out.println("sql exception caught");
                sqle.printStackTrace();
            }

            //***************************************************
            // Select using statement
            //***************************************************
            
            //Next select all the rows and display them here...
            ResultSet rs = stmt.executeQuery("select * from Booking");
            
            //Show all the bookers
            while (rs.next()) {
                
                System.out.println("Booker for id: "+rs.getString("id")+" is "+rs.getString("name1"));
                
                
//                int numColumns = rs.getMetaData().getColumnCount();
//                for (int i = 1; i <= numColumns; i++) {
//                    // Column numbers start at 1.
//                    // Also there are many methods on the result set to return
//                    //  the column as a particular type. Refer to the Sun documentation
//                    //  for the list of valid conversions.
//                    System.out.println("COLUMN " + i + " = " + rs.getObject(i));
//                }
            }
            



            //***************************************************
            // UPDATE with a prepared statement
            //Ask the user for an id and a new first name.  Update that record with 
            //the new name for the id entered.
            //***************************************************
            
            System.out.println("Enter id to update");
            Scanner input = new Scanner(System.in);
            int id = input.nextInt();
            input.nextLine();
            
            System.out.println("Enter new name of booker");
            String booker = input.nextLine();
            
            String thePreparedStatement = "UPDATE Booking set name1 = ? where id = ?";
            
            PreparedStatement preparedStatement = conn.prepareStatement(thePreparedStatement);
            try {
                preparedStatement.setString(1, booker);
                preparedStatement.setInt(2, id);
                preparedStatement.executeUpdate();

            } catch (SQLException sqle) {
                System.out.println("sql exception caught");
                sqle.printStackTrace();
            }
            
            
            
            
            
        } catch (SQLException sqle) {
            System.out.println("sql exception caught");
            System.out.println(sqle);
        } finally {
            //It's important to close the connection when you are done with it
            try {
                conn.close();
            } catch (Throwable e) {
                System.out.println("Could not close JDBC Connection" + e);
            }
        }
    }
}

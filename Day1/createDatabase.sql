drop database cis2232_teetime;
create database cis2232_teetime;
use cis2232_teetime;

drop table if exists Booking;

CREATE TABLE Booking (
	id int(5) PRIMARY KEY,
    name1 varchar(100) comment 'Booker name',
    name2 varchar(100) comment 'Player 2',
    name3 varchar(100) comment 'Player 3',
    name4 varchar(100) comment 'Player 4',
	bookingDate varchar(10) comment 'yyyy-MM-dd',
    bookingTime varchar(5) comment 'hh:mm',
    courseName varchar(100) comment 'Course name',
    createdDateTime varchar(20) DEFAULT NULL COMMENT 'When record was created.'
	);	

INSERT INTO `booking` (`id`, `name1`, `name2`, `name3`, `name4`, `bookingDate`, `bookingTime`, `courseName`, `createdDateTime`) VALUES ('1', 'Nidhiya Nair', 'Jeff Thistle', 'Bo Lan', 'Dylan Corriveau', '2020-05-18', '08:00', 'Summerside', '2020-05-14 15:07');

INSERT INTO `booking` (`id`, `name1`, `name2`, `name3`, `name4`, `bookingDate`, `bookingTime`, `courseName`, `createdDateTime`) VALUES ('2', 'Kendall Fowler', 'Omar Bakraky', 'Joey Abou Risk', 'Thao Ho', '2020-05-18', '08:10', 'Summerside', '2020-05-14 15:07');

INSERT INTO `booking` (`id`, `name1`, `name2`, `name3`, `name4`, `bookingDate`, `bookingTime`, `courseName`, `createdDateTime`) VALUES ('3', 'Nicole Ross', 'BJ MacLean', 'Mariana Alkabalan', '', '2020-05-18', '08:20', 'Summerside', '2020-05-14 15:07');